# First things first
This was written for [this post](https://blog.bydeley.ca/posts/polymer-tweet/) on my blog. See that for more information

# \<polymer-tweet\>

A demo component that consumes a siren tweet

## Install the Polymer-CLI

First, make sure you have the [Polymer CLI](https://www.npmjs.com/package/polymer-cli) installed. Then run `polymer serve` to serve your element locally.

## Viewing Your Element

```
$ polymer serve
```

## Running Tests

```
$ polymer test
```

Your application is already set up to be tested via [web-component-tester](https://github.com/Polymer/web-component-tester). Run `polymer test` to run your application's test suite locally.
